var db = new Kinto({bucket: 'coops-map'});
var coops = db.collection('coops');

var syncOptions = {
    remote: 'https://coops-map.herokuapp.com/v1/',
    strategy: Kinto.syncStrategy.CLIENT_WINS
}

var mymap = L.map('mapid').setView([12.200, 78.871], 7);
mymap.doubleClickZoom.disable();
var new_marker = null;
var marker = null;
var markerGroup = L.layerGroup().addTo(mymap);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);


function onMapClick(e) {
    if (new_marker != null) {
        mymap.removeLayer(new_marker);
    }
    // prepare a marker to display
    new_marker = L.marker(e.latlng, {draggable: false});
    // prepare a popup to display
    popup = L.popup();
    popup.setContent("You clicked here");
    // bind the popup with marker
    new_marker.bindPopup(popup).openPopup();
    // add the marker to map
    new_marker.addTo(mymap);
    // set the caption
    $('.header')[0].innerHTML = "Mark a Coop.";
    // reset form fields due to previous action
    $('#mark-a-coop')[0].reset();
    // fill the lat and lng in form
    $('#lat')[0].value = e.latlng.lat;
    $('#lon')[0].value = e.latlng.lng;
    // enable form fields if disabled by previous action
    form_elements = $('#mark-a-coop')[0].elements;
    for (i=0; i<form_elements.length; ++i){
        if (form_elements[i].readOnly === true)
            form_elements[i].readOnly = false;
    }
    // bring up the from to front
    $('.ui.modal').modal('show');
    // toggle action buttons
    $('#action-btns')[0].style.display = "block";
    $('#edit-btns')[0].style.display = "none";
}
mymap.on('dblclick', onMapClick);


document.getElementById('add')
    .addEventListener('click', function(event) {
        event.preventDefault();
        form = $('#mark-a-coop')[0];
        formData = {};
        // field validations
        if (form['name'].value.length <= 4)
            form['name'].focus();
        else if (form['lat'].value.length <= 4)
            form['lat'].focus();
        else if (form['lon'].value.length <= 4)
            form['lon'].focus();
        else {
            for (i=0; i<form.elements.length; i++){
                formData[form.elements[i].name] = form.elements[i].value;
            }
            coops.create(formData)
                .then(function(res) {
                    form.reset();
                    // remove temporary marker
                    mymap.removeLayer(new_marker);
                    // make form hidden and expand map size
                    $('.ui.modal').modal('hide');
                })
                .then(render)
                .then(syncServer)
                .catch(function(err) {
                    console.error(err);
                });
        }
    });


function resetView(){
    $('.header')[0].innerHTML = "Mark a Coop.";
    $('#mark-a-coop')[0].reset();
    $('.ui.modal').modal('hide');
}


document.getElementById('cancel')
    .addEventListener('click', function(event) {
        event.preventDefault();
        mymap.removeLayer(new_marker);
        resetView();
    });

document.getElementById('close')
    .addEventListener('click', resetView);


function syncServer() {
    console.log("Your network connection is: " + Offline.state);
    if (Offline.state === 'up') {
        coops.sync(syncOptions).then(function(res) {
            console.log(JSON.stringify(res, null, 2));
        })
            .then(render)
            .catch(function(err){
                console.error(err);
        });
    }
    else {
        console.info("Your network connections seems down. We will sync your data as soon as your connection is up.");
    }
}

function renderCoop(coop) {
    marker = L.marker({'lat': coop.lat, 'lng': coop.lon});
    popup_content = "<a href='#' onClick='showDetails(\"" + coop.id + "\")'>" + coop.name + "</a>";
    old_popup = L.popup();
    old_popup.setContent(popup_content);
    marker.bindPopup(old_popup).openPopup();
    marker.addTo(markerGroup);
}

function renderCoops(coops) {
    coops.forEach(function(coop) {
        renderCoop(coop);
    });
}

function render() {
    markerGroup.clearLayers();
    mymap.removeLayer(markerGroup);
    coops.list().then(function(res) {
        renderCoops(res.data);
        markerGroup.addTo(mymap);
    }).catch(function(err) {
        console.error(err);
    })
}

// render local data
render();
// try to sync and render
syncServer();


function showDetails() {
    coops.get(arguments[0])
        .then(function(coop) {
            form = document.forms['mark-a-coop'];
            form_div = document.getElementById('add-place');
            map_div = document.getElementById('mapid');
            // disable form fields
            form_elements = form.elements;
            for (i=0; i<form_elements.length; ++i){
                form_elements[i].readOnly = true;
            }
            // hide add & cancel button
            action_div = document.getElementById('action-btns');
            if (action_div.style.display !== "none")
                action_div.style.display = "none";
            // show edit actions
            edit_div = document.getElementById('edit-btns');
            if (edit_div.style.display !== "block")
                edit_div.style.display = "block";
            // change heading
            $('.header')[0].innerHTML = coop.data.name;
            // [TODO]show edit button
            for (key in coop.data) {
                if (key !== "id" && key !== "_status" && key !== "last_modified") {
                    // populate the form with existing data
                    form[key].value = coop.data[key];
                }
            }
            // bring back the info section
            $('.ui.modal').modal('show');
            }).catch(function(err) {
                console.error(err);
        });
}
